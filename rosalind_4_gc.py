

def makeDict(f):

    DNA_dict = {}
    key = ""
    data = ""

    strand = f.readline()
    while strand != "":
        key = strand[1:-1]
        strand = f.readline()
        while strand != "" and strand[0] != '>':
            data = data + strand[:-1]
            strand = f.readline()
        DNA_dict[key] = data
        data = ""

    return DNA_dict


def findGC(DNA_strand):

    GC_count = 0.0
    total = 0.0

    for c in DNA_strand:
        if c == 'G' or c == 'C':
            GC_count = GC_count + 1
        total = total + 1

    return (GC_count / total) * 100

highest_GC = 0
fasta = ""

filename = raw_input("Give me a filename")
f = open(filename, 'r')

DNA_dict = makeDict(f)

for name in DNA_dict.keys():
    GC_content = findGC(DNA_dict[name])
    if GC_content > highest_GC:
        highest_GC = GC_content
        fasta = name

print(fasta)
print(highest_GC)
