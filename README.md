A better means of organization will likely be implemented.  For now, the present organization will suffice.


rosalind_2_rna_code.py

Problem was basically to create an "RNA version" of a given DNA strand,
which basically involved replacing all the 'T's with 'U's.

rosalind_3_revc_code.py

A little bit tougher.  This time you had to take a given DNA strand, reverse it,
and then replace the characters with their respective base pair values.
(i.e. if you encounter an 'A', replace with a 'T', vise versa,
and if you encounter a 'G', replace with a 'C', vice versa).

rosalind_4_gc.py

Likely the breaking point for n00b/the unmotivated.
Here, you read a file written in FASTA format, and in my code example, organize data
into a dictionary, with names as keys and strands as values (this probably isn't necessary).
Once done, a seperate function determines the percentages of 'G's and 'C's relative to everything else.

Interestingly enough, with a long enough unknown DNA strand, you can get a good idea as to
what species it came from, given the GC content.
