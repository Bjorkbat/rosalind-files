def dna_to_rna(dna_str):
	rna_str = ""
	for i in dna_str:
		if i == 'A':
			rna_str = rna_str + 'A'
		elif i == 'T':
			rna_str = rna_str + 'U'
		elif i == 'C':
			rna_str = rna_str + 'C'
		elif i == 'G':
			rna_str = rna_str + 'G'
	return rna_str

f = open('/home/bjorkbat/Downloads/rosalind_rna.txt', 'r')
dna = f.read()

rna = dna_to_rna(dna)

f2 = open('answer2.txt', 'w')
f2.write(rna)
