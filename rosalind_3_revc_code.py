def rev_comp(dna_str):
	rev_strand = ""
	rev_c = ""

	for c in dna_str:
		if c == 'A':
			rev_c = "T"
			rev_strand = rev_c + rev_strand
		elif c == 'T':
			rev_c = "A"
			rev_strand = rev_c + rev_strand
		elif c == 'C':
			rev_c = "G"
			rev_strand = rev_c + rev_strand
		elif c == 'G':
			rev_c = "C"
			rev_strand = rev_c + rev_strand
	return rev_strand

f_read = open("/home/bjorkbat/Downloads/rosalind_revc.txt", 'r')
dna = f_read.read()

rev_dna = rev_comp(dna)
f_write = open("project3_answer.txt", 'w')
f_write.write(rev_dna)
